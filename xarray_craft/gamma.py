'''
This is an object to take care of unit vector
'''
from xarrayuvecs.uniform_dist import unidist
import xarrayuvecs.lut2d as lut2d

import datetime
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from sklearn.neighbors import KernelDensity
import scipy

@xr.register_dataarray_accessor("gamma")

class gamma(object):
    '''
    This is a classe to work on slip system output from CraFT
    
    .. note:: xarray does not support heritage from xr.DataArray may be the day it support it, we could move to it
    '''
    
    def __init__(self, xarray_obj):
        '''
        :param xarray_obj: dimention should be (n,m,12)
        :type xarray_obj: xr.DataArray
        '''
        self._obj = xarray_obj 
    pass

    def gamma_activity(self,plane='ba',norm=True):
        '''
        :param plane: relative activity of 'ba' for basal, 'pr' for prismatic, 'py' for pyramidal
        :type plane: str
        :return: relative basal activity map
        :rtype: im2d.image2d
        '''
        if norm:
            nn=np.sum(np.float128(self._obj)**2,axis=-1)
        else:
            nn=1.
            
        if plane=='ba':
            res=np.sum(np.float128(self._obj**2)[...,0:3],axis=-1)/nn
        elif plane=='pr':
            res=np.sum(np.float128(self._obj**2)[...,3:6],axis=-1)/nn
        elif plane=='py':
            res=np.sum(np.float128(self._obj**2)[...,6:12],axis=-1)/nn
            
        return xr.DataArray(res,dims=self._obj.coords.dims[0:-1])
