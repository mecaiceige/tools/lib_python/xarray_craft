Functions overview
==================

Loading data
************
.. automodule:: xarray_craft.loadData_craft
	:special-members:
	:members:

Craft xr.Dataset accessor
*************************
.. automodule:: xarray_craft.craft
	:special-members:
	:members:

Gamma xr.DataArray accessor
***************************
.. automodule:: xarray_craft.gamma
	:special-members:
	:members:
