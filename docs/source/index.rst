.. xarray_craft documentation master file, created by
   sphinx-quickstart on Mon Aug  2 14:46:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xarray_craft's documentation!
==============================================

``xarray_craft`` is a `xarray <http://xarray.pydata.org/en/stable/>`_ accesors to work on `CraFT <https://lma-software-craft.cnrs.fr/>`_ output for ice simulation.

In this documentation detail of the functions are given. For quick start script to use this accesors we recommend the `CraFT Book <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/CraFT-Book>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Installation
============

From repository
***************

.. code:: bash
    
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_craft
    cd xarray_craft
    python -m pip install -r requirements.txt
    pip install -e .

Functions Overview
==================

.. toctree::
    :maxdepth: 0
    :numbered: 
    
    func_o


Dependancies
============

This ``xarray_craft`` module as few dependencies :

1. `xarray_aita <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_aita/>`_ for managing CraFT input data 
2. `xarray_symTensor2d <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_symTensor2d>_` for managing symmetric tensor 3x3 such as stress and strain tensor
3. `xarray_uvecs <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/lib_python/xarray_uvecs/>`_ for managing Unit VECtor that are Symmetric.


Contact
=======
:Author: Thomas Chauve
:Contact: thomas.chauve@univ-grenoble-alpes.fr

:Organization: `IGE <https://www.ige-grenoble.fr/>`_
:Status: This is a "work in progress"
:Version: 0.1

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
